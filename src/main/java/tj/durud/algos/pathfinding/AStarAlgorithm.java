package tj.durud.algos.pathfinding;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;

import static java.lang.Math.abs;
import static java.util.Comparator.comparingDouble;

public class AStarAlgorithm {


    /**
     * **s************
     * ***************
     * ****########***
     * *********g*****
     */
    public static void main(String[] args) {

        char[][] maze = {
                "**s******************************************************".toCharArray(),
                "#####*###################################################*".toCharArray(),
                "********************************************************".toCharArray(),
                "*######*##################################################".toCharArray(),
                "*#######*#################################################*".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####***********************g***************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "***####**************************************************".toCharArray(),
                "*###########*********************************************".toCharArray(),
                "********************************************************".toCharArray(),
        };

        Cell start = findCell(maze, 's');
        Cell goal = findCell(maze, 'g');


        PriorityQueue<Cell> open = new PriorityQueue<>(comparingDouble(Cell::fCost));

        List<Cell> closed = new ArrayList<>();
        open.add(start);

        while (!open.isEmpty()) {
            Cell cell = open.poll();

            for (Cell successor : getNeighbors(maze, cell)) {
                successor.prev = cell;
                if (successor.val == 'g') {
                    goal.prev = cell;
                    break;
                }
                successor.calculateGCost();
                successor.calculateHCost(goal);

                if (closed.contains(successor)) {
                    int ind = closed.indexOf(successor);
                    if (closed.get(ind).fCost() < successor.fCost()) {
                        continue;
                    }
                }

                if (open.contains(successor)) {
                    Cell existing = null;
                    for (Cell c : open) {
                        if (c.equals(successor)) {
                            existing = c;
                        }
                    }
                    if (existing.fCost() > successor.fCost()) {
                        open.remove(existing);
                    }
                } else {
                    open.add(successor);
                }
            }
            closed.add(cell);

            if (goal.prev != null) { //ugly for now
                break;
            }
        }

        Cell runner = goal;
        while (runner.prev != null && !runner.prev.equals(start)) {
            runner = runner.prev;
            maze[runner.x][runner.y] = '+';
        }

        print(maze);

        System.out.println("========================");

        for (Cell c : closed) {
            if (maze[c.x][c.y] == '*' || maze[c.x][c.y] == '+' || maze[c.x][c.y] == 's') {
                maze[c.x][c.y] = '1';
            } else {
                int v = Integer.parseInt("" + maze[c.x][c.y]) + 1;
                if (v > 9) {
                    maze[c.x][c.y] = '!';
                } else {
                    maze[c.x][c.y] = String.valueOf(v).toCharArray()[0];
                }
            }
        }
        print(maze);
    }


    private static void print(char[][] maze) {
        for (char[] chars : maze) {
            System.out.println(new String(chars));
        }
    }

    private static Cell[] getNeighbors(char[][] maze, Cell cell) {

        int[] xs = {cell.x - 1, cell.x - 1, cell.x - 1, cell.x + 1, cell.x + 1, cell.x + 1, cell.x, cell.x};
        int[] ys = {cell.y - 1, cell.y, cell.y + 1, cell.y - 1, cell.y, cell.y + 1, cell.y - 1, cell.y + 1};

        List<Cell> list = new ArrayList<>();

        for (int i = 0; i < ys.length; i++) {
            int x = xs[i];
            int y = ys[i];
            if (validCell(maze, x, y)) {
                list.add(new Cell(x, y, maze[x][y]));
            }
        }

        return list.toArray(new Cell[1]);
    }

    private static boolean validCell(char[][] maze, int x, int y) {
        return x >= 0 && y >= 0 && x < maze.length && y < maze[x].length && maze[x][y] != '#' && maze[x][y] != 's';
    }

    private static Cell findCell(char[][] maze, char s) {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                if (maze[i][j] == s) {
                    return new Cell(i, j, maze[i][j]);
                }
            }
        }
        throw new IllegalStateException("The searched element doesn't exist");
    }


    private static class Cell {
        char val;
        int x, y;
        double gCost = 0; // distance from start to here
        double hCost = 0; // distance from here to goal (heuristic)
        Cell prev = null;

        Cell(int x, int y, char val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        public double fCost() {
            return gCost + hCost;
        }

        public void calculateGCost() {
            Objects.requireNonNull(prev);
            if (isDiagonal(this, prev)) {
                gCost = this.prev.gCost + 1.5;
            } else {
                gCost = this.prev.gCost + 1;
            }
        }

        private boolean isDiagonal(Cell c1, Cell c2) {
            return Math.abs(c1.x - c2.x) == 1 && Math.abs(c1.y - c2.y) == 1;
        }

        public void calculateHCost(Cell goal) {
            hCost = Math.max(abs(goal.x - this.x), abs(goal.y - this.y));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cell cell = (Cell) o;
            return x == cell.x &&
                    y == cell.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "Cell{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
