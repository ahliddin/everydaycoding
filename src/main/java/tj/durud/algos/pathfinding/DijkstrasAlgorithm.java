package tj.durud.algos.pathfinding;

import java.util.*;

class Graph {
    List<List<Vertex>> adjacents;
    private Vertex start;
    List<Edge> edges;
    Map<Integer, Vertex> processedVertices = new HashMap<>();

    public Graph(List<Edge> edges) {
        this.edges = edges;
        start = new Vertex(0);
        start.shortestPath = 0;

        adjacents = new ArrayList<>(edges.size());

        for (int i = 0; i < edges.size(); i++) {
            adjacents.add(new ArrayList<>());
        }

        for (Edge edge : edges) {
            adjacents.get(edge.start).add(new Vertex(edge.end));
        }
    }

    public static void main(String[] args) {

        List<Edge> edges = Arrays.asList(
                new Edge(0, 1, 1),
                new Edge(0, 5, 8),
                new Edge(1, 2, 2),
                new Edge(1, 3, 4),
                new Edge(2, 4, 2),
                new Edge(3, 4, 3),
                new Edge(4, 5, 1)
        );


        Graph graph = new Graph(edges);

        Vertex vertex = graph.shortestPath(new Vertex(0), new Vertex(5));
        System.out.println(vertex);
    }

    public Vertex shortestPath(Vertex src, Vertex dest) {
        resetGraph();

        calculatePaths(src);

        return processedVertices.get(dest.val);
    }

    private void calculatePaths(Vertex src) {

        Queue<Vertex> queue = new ArrayDeque<>();
        queue.add(src);

        while (!queue.isEmpty()) {
            Vertex vertex = queue.remove();

            for (Vertex v : getAdjacents(vertex)) {
                int size = calculatePathWeight(vertex, v);
                if (size < v.shortestPath) {
                    v.shortestPath = size;
                    v.prev = vertex;
                }
                queue.add(v);
                processedVertices.put(v.val, v);
            }
        }
    }

    private int calculatePathWeight(Vertex src, Vertex dest) {
        for (Edge e : edges) {
            if (e.start == src.val && e.end == dest.val) {
                return src.shortestPath + e.weight;
            }
        }
        throw new IllegalStateException();
    }

    private void resetGraph() {
        processedVertices.clear();
        for (List<Vertex> vertices : adjacents) {
            for (Vertex v : vertices) {
                v.shortestPath = Integer.MAX_VALUE;
                v.prev = null;
            }
        }
    }

    public List<Vertex> getAdjacents(Vertex vertex) {
        return adjacents.get(vertex.val);
    }
}

class Vertex {
    int val;
    Vertex prev;
    int shortestPath;

    public Vertex(int val) {
        this.val = val;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return val == vertex.val;
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "val=" + val +
                ", prev=" + prev +
                ", shortestPath=" + shortestPath +
                '}';
    }
}

class Edge {
    int start;
    int end;
    int weight;

    public Edge(int start, int end, int weight) {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }
}