package tj.durud.algos;


import com.github.afkbrb.btp.BTPrinter;

import java.util.*;

public class BinarySearchTree {
    public static final String EMPTY_NODE = "_";
    private Node root;

    public BinarySearchTree(Node root) {
        this.root = root;
    }

    public static void main(String[] args) {

        Node root = new Node(1);
        Node runner = root;
        for (int i = 2; i < 10; i++) {
            runner = runner.addChild(new Node(i));
        }

        BinarySearchTree bst = new BinarySearchTree(root);
        bst.print();
        bst.rebalance();
        bst.print();
    }


    public void print() {
        resetIndices();
        String str = Arrays.toString(toArray())
                .replaceAll("0", "#")
                .replace("[", "")
                .replace("]", "")
                .replace(" ", "");

        System.out.println(str);
        BTPrinter.printTreeLevelOrder(str);

    }


    public void rebalance() {

        List<Node> sortedList = toSortedList();
        System.out.println("sorted list: " + sortedList);

        Node[] arr = sortedList.toArray(new Node[]{});

        root = setMediumRoot(arr, 0, arr.length - 1);
    }

    private Node setMediumRoot(Node[] arr, int start, int end) {

        if (end < start) {
            return null;
        }
        int middle = (start + end) / 2;
        Node root = arr[middle];

        root.left = setMediumRoot(arr, start, middle - 1);
        root.right = setMediumRoot(arr, middle + 1, end);

        return root;
    }

    private int[] toArray() {
        int[] arr = new int[20];

        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if (node.index >= arr.length) {
                arr = doubleSize(arr);
            }
            arr[node.index] = node.value;
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }

        return arr;
    }

    private int[] doubleSize(int[] arr) {
        int[] newArr = new int[arr.length * 2];
        System.arraycopy(arr, 0, newArr, 0, arr.length);
        return newArr;
    }

    private void resetIndices() {
        calculateIndex(root, 0);
    }

    private void calculateIndex(Node node, int index) {
        if (node == null) {
            return;
        }
        node.index = index;
        calculateIndex(node.left, index * 2 + 1);
        calculateIndex(node.right, index * 2 + 2);
    }


    private List<Node> toSortedList() {
        List<Node> list = new ArrayList<>();
        inorderTraverse(list, root);
        list.forEach(n -> {
            n.index = 0;
            n.left = null;
            n.right = null;
        });
        return list;
    }

    private void inorderTraverse(List<Node> list, Node node) {
        if (node == null) {
            return;
        }
        inorderTraverse(list, node.left);
        list.add(node);
        inorderTraverse(list, node.right);

    }


    private static class Node {
        int value;
        int index;
        Node left;
        Node right;

        public Node(int val) {
            this.value = val;
            this.index = 0;
        }

        public Node addChild(Node child) {
            if (child.value < this.value) {
                left = child;
            } else {
                right = child;
            }
            return child;
        }

        public Node addLeft(Node left) {
            this.left = left;
            return this.left;
        }

        public Node addRight(Node right) {
            this.right = right;
            return this.right;
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

}
