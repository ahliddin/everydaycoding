package tj.durud.hfdp.decorator;


import tj.durud.hfdp.decorator.kavarna.*;

/**
 * Gang of Four definition:
 * Attach additional responsibilities to an object dynamically.
 * Decorators provide a flexible alternative to subclassing for extending functionality.
 * Also known as wrapper.
 */
public class DecoratorDemo {

    public static void main(String[] args) {
        Beverage drink = new WhipDecorator(new MilkDecorator(new Espresso()));
        Beverage drink2 = new MilkDecorator(new MilkDecorator(new WhipDecorator(new Cappuccino())));

        System.out.println(drink.description());
        System.out.println("Total: " + drink.cost());

        System.out.println("---------");

        System.out.println(drink2.description());
        System.out.println("Total: " + drink2.cost());

    }

}

