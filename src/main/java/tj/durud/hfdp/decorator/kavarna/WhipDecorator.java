package tj.durud.hfdp.decorator.kavarna;

public class WhipDecorator implements Beverage {
    private int price = 5;
    private Beverage beverage;

    public WhipDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public int cost() {
        return beverage.cost() + price;
    }

    @Override
    public String description() {
        return beverage.description() + "\nwhip - " + price + ",-";
    }
}
