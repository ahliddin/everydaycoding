package tj.durud.hfdp.decorator.kavarna;

public class Espresso implements Beverage {
    @Override
    public int cost() {
        return 45;
    }

    @Override
    public String description() {
        return "Espresso - " + cost() + ",- ";
    }
}
