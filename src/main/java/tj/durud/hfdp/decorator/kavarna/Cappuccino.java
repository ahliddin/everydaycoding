package tj.durud.hfdp.decorator.kavarna;

public class Cappuccino implements Beverage {

    @Override
    public int cost() {
        return 55;
    }

    @Override
    public String description() {
        return "Cappuccino - " + cost() + ",-";
    }
}
