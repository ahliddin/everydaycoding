package tj.durud.hfdp.decorator.kavarna;

public interface Beverage {

    int cost();

    String description();
}
