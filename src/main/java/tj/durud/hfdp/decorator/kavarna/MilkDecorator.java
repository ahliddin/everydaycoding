package tj.durud.hfdp.decorator.kavarna;

public class MilkDecorator implements Beverage {
    private int price = 10;
    private Beverage beverage;

    public MilkDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public int cost() {
        return beverage.cost() + price;
    }

    @Override
    public String description() {
        return beverage.description() + "\nmilk - " + price + ",-";
    }
}
