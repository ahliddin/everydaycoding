package tj.durud.hfdp.observer;

public class WeatherData {

    private int temperature;
    private double pressure;
    private double humidity;

    public WeatherData(int temperature, double pressure, double humidity) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public int getTemperature() {
        return temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    @Override
    public String toString() {
        return "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity;
    }
}
