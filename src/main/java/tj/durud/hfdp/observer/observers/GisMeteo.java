package tj.durud.hfdp.observer.observers;

import tj.durud.hfdp.observer.WeatherData;
import tj.durud.hfdp.observer.subject.Subject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GisMeteo implements Observer {

    private WeatherData weatherData;
    private Subject subject;

    public GisMeteo(Subject subject) {
        this.subject = subject;
        this.subject.register(this);
    }

    @Override
    public void update(WeatherData data) {
        weatherData = data;
        display();
    }

    public void display() {
        System.out.println("GISMETEO - " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME) + " - " + weatherData);

    }
}
