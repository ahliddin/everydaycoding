package tj.durud.hfdp.observer.observers;

import tj.durud.hfdp.observer.WeatherData;
import tj.durud.hfdp.observer.subject.Subject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WeatherDotCom implements Observer {
    private WeatherData weatherData;
    private Subject subject;


    public WeatherDotCom(Subject subject) {
        this.subject = subject;
        this.subject.register(this);
    }

    @Override
    public void update(WeatherData data) {
        this.weatherData = data;
        display();
    }

    private void display() {
        System.out.println("Weather.com - " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME) + " - " + weatherData);
    }
}
