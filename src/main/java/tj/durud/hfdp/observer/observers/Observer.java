package tj.durud.hfdp.observer.observers;

import tj.durud.hfdp.observer.WeatherData;

public interface Observer {

    void update(WeatherData data);
}
