package tj.durud.hfdp.observer.subject;

import tj.durud.hfdp.observer.WeatherData;
import tj.durud.hfdp.observer.observers.Observer;

public interface Subject {

    void notifyObservers(WeatherData data);

    void register(Observer observer);

    void unregister(Observer observer);

}
