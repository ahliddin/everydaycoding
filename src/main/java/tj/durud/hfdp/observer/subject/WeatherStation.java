package tj.durud.hfdp.observer.subject;

import tj.durud.hfdp.observer.WeatherData;
import tj.durud.hfdp.observer.observers.Observer;

import java.util.ArrayList;
import java.util.List;

public class WeatherStation implements Subject {

    private List<Observer> observers = new ArrayList<>();

    @Override
    public void notifyObservers(WeatherData newData) {
        for (Observer observer : observers) {
            observer.update(newData);
        }
    }

    @Override
    public void register(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        observers.remove(observer);

    }

    public void weatherChanged(WeatherData weatherData) {
        notifyObservers(weatherData);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
