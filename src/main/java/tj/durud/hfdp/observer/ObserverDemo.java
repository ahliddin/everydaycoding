package tj.durud.hfdp.observer;


import tj.durud.hfdp.observer.observers.GisMeteo;
import tj.durud.hfdp.observer.observers.WeatherDotCom;
import tj.durud.hfdp.observer.subject.WeatherStation;

/**
 * Gang of Four definition:
 * Define a one-to-many dependency between objects so that when one object changes state,
 * all its dependents are notified and updated automatically.
 * <p>
 * Aso known as: Dependents, Publish-Subscribe
 * <p>
 * Head First Design Patterns definition:
 * If you understand the newspaper subscription, than you pretty much understand the Observer pattern.
 * Only we call the publisher the SUBJECT and subscribers the OBSERVERS.
 */
public class ObserverDemo {

    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation();

        GisMeteo gisMeteo = new GisMeteo(weatherStation);
        WeatherDotCom weatherDotCom = new WeatherDotCom(weatherStation);

        weatherStation.weatherChanged(new WeatherData(15, 140.5, 71.0));
        weatherStation.weatherChanged(new WeatherData(14, 134, 70.0));
        weatherStation.weatherChanged(new WeatherData(16, 164.5, 75.0));
    }


}
