package tj.durud.hfdp.command;


/**
 * Gang of Four definition:
 * Encapsulate a request as an object, thereby letting you parameterize clients with different requests,
 * queue or log requests, and support undoable operations.
 * <p>
 * CommandDemo acts as the Client
 * <p>
 * Client is responsible for creating a ConcreteCommand and setting its Receiver
 */
public class CommandDemo {
    public static void main(String[] args) {

        Command command = new ConcreteCommand(new Receiver());
        Invoker invoker = new Invoker();

        invoker.setCommand(command);

        invoker.invokeOperation();


    }
}
