package tj.durud.hfdp.command;

public class ConcreteCommand implements Command {
    private Receiver receiver;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        System.out.println("Executing the ConcreteCommand");
        receiver.introduceYouself();
        receiver.action();
        receiver.anotherAction();
        receiver.sayGoodBuy();


    }
}
