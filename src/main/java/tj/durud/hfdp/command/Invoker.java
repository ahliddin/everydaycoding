package tj.durud.hfdp.command;

public class Invoker {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void invokeOperation() {
        command.execute();
    }

}
