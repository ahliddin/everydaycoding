package tj.durud.hfdp.command;

public interface Command {
    void execute();
}
