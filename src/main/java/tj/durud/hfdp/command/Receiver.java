package tj.durud.hfdp.command;

/**
 * Knows how to perform the actual work. It is executed by the ConcreteCommand
 */
public class Receiver {


    public void action() {
        System.out.println("I'm the the final point in the whole bloody chain.");
        System.out.println("I'm the guy who does the real job. And I'm decoupled from any Client.");
    }

    public void introduceYouself() {
        System.out.println("I am a Receiver, i can perform multiple actions. ConcreteCommand decides which actions to perform");
    }

    public void anotherAction() {
        System.out.println("One more actions by Receiver");
    }

    public void sayGoodBuy() {
        System.out.println("All actions are performed. Enough is enough!");
    }
}
