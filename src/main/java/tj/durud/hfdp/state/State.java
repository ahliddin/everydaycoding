package tj.durud.hfdp.state;

public interface State {

    void insertQuarter();

    void ejectQuarter();

    void turnCrank();

}
