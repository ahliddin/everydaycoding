package tj.durud.hfdp.state;

public class QuarterInsertedState implements State {

    private GumballMachine gumballMachine;

    public QuarterInsertedState(GumballMachine machine) {
        gumballMachine = machine;
    }


    @Override
    public void insertQuarter() {
        System.out.println("Quarter is already inserted..");

    }

    @Override
    public void ejectQuarter() {
        System.out.println("Quarter ejected");
        gumballMachine.setState(gumballMachine.noQuarterState);
    }

    @Override
    public void turnCrank() {
        if (gumballMachine.getCount() > 0) {
            System.out.println("Crank is turned catch your gumball!");
            gumballMachine.setState(gumballMachine.noQuarterState);
            gumballMachine.decreaseCount();

        } else {
            System.out.println("No gumballs are left. Sorry, but we need to return your money");
            ejectQuarter();
        }
    }
}
