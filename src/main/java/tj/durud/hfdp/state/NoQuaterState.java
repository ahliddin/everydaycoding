package tj.durud.hfdp.state;

public class NoQuaterState implements State {

    private GumballMachine gumballMachine;

    public NoQuaterState(GumballMachine machine) {
        gumballMachine = machine;
    }


    @Override
    public void insertQuarter() {
        System.out.println("Inserting quarter..");
        gumballMachine.setState(gumballMachine.quarterInsertedState);
    }

    @Override
    public void ejectQuarter() {
        System.out.println("Quarter cannot be ejected in NoQuarterState");

    }

    @Override
    public void turnCrank() {
        System.out.println("Crank cannot be turned in NoQuarterState");

    }
}
