package tj.durud.hfdp.state;

public class GumballMachine {
    private int count;
    private State currentState;

    public final State quarterInsertedState = new QuarterInsertedState(this);
    public final State noQuarterState = new NoQuaterState(this);

    public GumballMachine(int count) {
        this.count = count;
        this.currentState = noQuarterState;
    }

    public void insertQuarter() {
        currentState.insertQuarter();
    }

    public void ejectQuarter() {
        currentState.ejectQuarter();
    }

    public void turnCrank() {
        currentState.turnCrank();
    }


    public void setState(State state) {
        currentState = state;
    }

    public int getCount() {
        return count;
    }

    public void decreaseCount() {
        this.count--;
    }
}
