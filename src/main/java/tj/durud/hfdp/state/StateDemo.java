package tj.durud.hfdp.state;

/**
 * Gang od Four definition:
 * Allow an object to alter its behavior when its internal state changes.
 * The object will appear to change its class.
 */
public class StateDemo {

    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(3);


        gumballMachine.turnCrank();
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println("-----------");
        gumballMachine.insertQuarter();
        gumballMachine.ejectQuarter();
        gumballMachine.ejectQuarter();

        System.out.println("-----------");
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
        gumballMachine.turnCrank();

        System.out.println("-----------");
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println("-----------");
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
    }
}
