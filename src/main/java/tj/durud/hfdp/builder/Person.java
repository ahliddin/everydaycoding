package tj.durud.hfdp.builder;

import java.time.LocalDate;

public class Person {

    private String name = "";
    private String lastName = "";
    private String address = "";
    private LocalDate birthday;


    public Person(String name, String lastName, String address, LocalDate birthday) {
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.birthday = birthday;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
