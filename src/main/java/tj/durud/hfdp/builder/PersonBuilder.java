package tj.durud.hfdp.builder;

import java.time.LocalDate;

public class PersonBuilder {

    private String name = "";
    private String lastName = "";
    private String address = "";
    private LocalDate birthday;


    private PersonBuilder() {

    }


    public PersonBuilder setName(String name) {
        this.name = name;

        return this;
    }

    public PersonBuilder setLastName(String lastName) {
        this.lastName = lastName;

        return this;
    }

    public PersonBuilder setAddress(String address) {
        this.address = address;

        return this;
    }

    public PersonBuilder setBirthday(LocalDate birthday) {
        this.birthday = birthday;

        return this;
    }

    public Person build() {
        return new Person(name, lastName, address, birthday);
    }

    public static PersonBuilder instance() {
        return new PersonBuilder();
    }
}
