package tj.durud.hfdp.builder;

import java.time.LocalDate;


/**
 * Gang of Four definition:
 * Separate the construction of a complex object from its representation so that the same consturction process can create different representations.
 * <p>
 * Use case from Bloch:
 * To avoid the 'telescope' constructors - multiple constructors with different number of parameters and lots of possible default field values.
 */
public class PersonBuilderDemo {

    public static void main(String[] args) {


        Person mainPerson = PersonBuilder.instance()
                .setName("Idris")
                .setLastName("Ibragimov")
                .setAddress("Choceradska 3118/4, Prague")
                .setBirthday(LocalDate.of(2018, 11, 14))
                .build();


        System.out.println(mainPerson);


    }
}
