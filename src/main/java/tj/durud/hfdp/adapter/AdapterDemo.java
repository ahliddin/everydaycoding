package tj.durud.hfdp.adapter;


import tj.durud.hfdp.adapter.duckexample.Duck;
import tj.durud.hfdp.adapter.duckexample.MallardDuck;
import tj.durud.hfdp.adapter.duckexample.Turkey;
import tj.durud.hfdp.adapter.duckexample.TurkeyAdapter;
import tj.durud.hfdp.adapter.iteratorexample.EnumerationIterator;

import java.util.Enumeration;
import java.util.Iterator;

import static java.util.Arrays.asList;
import static java.util.Collections.enumeration;

/**
 * Gang of Four definition:
 * Convert the interface of a class into another interface clients expect.
 * Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.
 * Also knows as Wrapper.
 *
 */

public class AdapterDemo {

    public static void main(String[] args) {

        duckDemo();

        bitMoreSeriousDemo();

    }

    private static void bitMoreSeriousDemo() {

        Enumeration<String> enumeration = enumeration(asList("Hello", "Bye-bye"));

        Iterator<String> iterator = new EnumerationIterator<>(enumeration);

        while (iterator.hasNext()) {
            System.out.println("Iterator: " + iterator.next());
        }

    }

    private static void duckDemo() {
        Duck duck = new MallardDuck();
        duck.fly();
        duck.quack();

        Duck duckTurkey = new TurkeyAdapter(new Turkey());
        duckTurkey.fly();
        duckTurkey.quack();
    }


}
