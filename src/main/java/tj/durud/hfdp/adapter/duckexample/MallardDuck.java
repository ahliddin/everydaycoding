package tj.durud.hfdp.adapter.duckexample;

public class MallardDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("Duck: quack-quack!");
    }

    @Override
    public void fly() {
        System.out.println("Duck: flying far far away.");
    }
}
