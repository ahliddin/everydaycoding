package tj.durud.hfdp.adapter.duckexample;

public class Turkey {

    public void fly() {
        System.out.println("Turkey: can't fly much");
    }

    public void gobble() {
        System.out.println("Turkey: I say gobble-gobble!");
    }
}
