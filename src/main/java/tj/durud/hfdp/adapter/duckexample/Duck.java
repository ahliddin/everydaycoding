package tj.durud.hfdp.adapter.duckexample;

public interface Duck {

    void quack();

    void fly();

}
