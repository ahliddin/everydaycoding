package tj.durud.hfdp.chain;

public class Manager extends PurchaseAuthorizer {

    public Manager() {
        this.allowable = BASE * 100;
    }

    @Override
    public void processPayment(Payment payment) {
        if (payment.getAmount() < allowable) {
            System.out.println("Manager is allowed to process the payment: " + payment.getDesc());
            System.out.println("Manager has processed the payment");

        } else if (supervisor != null) {
            System.out.println("Manager is not allowed to process it. Trying supervisor.");
            supervisor.processPayment(payment);

        } else {
            System.out.println("Payment cannot be processed. It's not allowed and Manager doesn't have supervisor defined.");
        }

    }


}
