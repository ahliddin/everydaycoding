package tj.durud.hfdp.chain;

public class Clerk extends PurchaseAuthorizer {

    public Clerk() {
        this.allowable = BASE * 10;
    }

    @Override
    public void processPayment(Payment payment) {
        if (payment.getAmount() < allowable) {
            System.out.println("Clerk is allowed to process the payment: " + payment.getDesc());
            System.out.println("Clerk has processed the payment");

        } else if (supervisor != null) {
            System.out.println("Clerk is not allowed to process it. Trying supervisor.");
            supervisor.processPayment(payment);

        } else {
            System.out.println("Payment cannot be processed. It's not allowed and Clerk doesn't have supervisor defined.");
        }

        System.out.println("-------------");

    }


}
