package tj.durud.hfdp.chain;


public abstract class PurchaseAuthorizer {

    protected final int BASE = 500;

    protected int allowable;
    protected PurchaseAuthorizer supervisor;

    public abstract void processPayment(Payment payment);

    public int getAllowable() {
        return allowable;
    }

    public void setSupervisor(PurchaseAuthorizer supervisor) {
        this.supervisor = supervisor;
    }

}
