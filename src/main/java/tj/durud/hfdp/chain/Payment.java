package tj.durud.hfdp.chain;

public class Payment {

    private int amount;
    private String desc;

    public Payment(int amount, String desc) {
        this.amount = amount;
        this.desc = desc;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
