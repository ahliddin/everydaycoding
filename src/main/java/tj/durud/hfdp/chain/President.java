package tj.durud.hfdp.chain;

public class President extends PurchaseAuthorizer {

    public President() {
        this.allowable = BASE * 1000;
    }

    @Override
    public void processPayment(Payment payment) {
        if (payment.getAmount() < allowable) {
            System.out.println("President is allowed to process the apyment: " + payment.getDesc());
            System.out.println("President has processed the payment");

        } else if (supervisor != null) {
            System.out.println("President is not allowed to process it. Trying supervisor.");
            supervisor.processPayment(payment);

        } else {
            System.out.println("Payment cannot be processed. It's not allowed and President doesn't have supervisor defined.");
        }

    }


}
