package tj.durud.hfdp.chain;


/**
 * Gang of Four definition:
 * Avoid coupling the sender of a request to its receiver
 * by giving more than one object a chance to handle the request.
 * Chain the receiving objects and pass the request along the chain until an object handles it.
 */
public class ChainOfResponsibilityDemo {

    public static void main(String[] args) {
        PurchaseAuthorizer president = new President();
        PurchaseAuthorizer manager = new Manager();
        PurchaseAuthorizer clerk = new Clerk();

        clerk.setSupervisor(manager);
        manager.setSupervisor(president);

        clerk.processPayment(new Payment(50, "Raspberry Pi"));
        clerk.processPayment(new Payment(5500, "Shiny work chair"));
        clerk.processPayment(new Payment(50000, "Cool family car"));
        clerk.processPayment(new Payment(5000000, "One more investment flat"));

    }
}
