package tj.durud.hfdp.factory.abstractfactory;

public class Pizza {
    private String name;
    private String cheese;
    private String topping;

    private PizzaFactory factory;

    public Pizza(PizzaFactory factory) {
        this.factory = factory;
    }

    public void prepare() {
        name = factory.name();
        cheese = factory.createCheese();
        topping = factory.createToppings();

    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", cheese='" + cheese + '\'' +
                ", topping='" + topping + '\'' +
                '}';
    }
}
