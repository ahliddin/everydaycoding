package tj.durud.hfdp.factory.abstractfactory;

public class NyPizzaFactory implements PizzaFactory {
    @Override
    public String name() {
        return "NY special pizza";
    }

    @Override
    public String createCheese() {
        return "NY special cheese";
    }

    @Override
    public String createToppings() {
        return "NY special toppings";
    }

}
