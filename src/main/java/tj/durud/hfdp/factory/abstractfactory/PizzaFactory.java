package tj.durud.hfdp.factory.abstractfactory;

public interface PizzaFactory {
    String name();

    String createCheese();

    String createToppings();

}
