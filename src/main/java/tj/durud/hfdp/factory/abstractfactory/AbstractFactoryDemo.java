package tj.durud.hfdp.factory.abstractfactory;

/**
 * Gang of Four definition:
 * Provide an interface for creating families of related or dependent objects
 * without specifying their concrete classes.
 * <p>
 * Also known as: Kit
 */
public class AbstractFactoryDemo {

    public static void main(String[] args) {
        PizzaFactory factory = new NyPizzaFactory();

        Pizza pizza = new Pizza(factory);
        pizza.prepare();

        System.out.println(pizza.toString());
    }
}
