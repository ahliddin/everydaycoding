package tj.durud.hfdp.factory.factorymethod;

import tj.durud.hfdp.factory.factorymethod.pizza.Pizza;
import tj.durud.hfdp.factory.factorymethod.pizza.PizzaType;
import tj.durud.hfdp.factory.factorymethod.store.NYStylePizzaStore;
import tj.durud.hfdp.factory.factorymethod.store.PizzaStore;

/**
 * Gang of Four definition:
 * Define an interface for creating an object, but let subclasses decide which class to instantiate.
 * Factory Method lets a class defer instantiation to subclasses.
 */
public class FactoryMethodDemo {

    public static void main(String[] args) {
        PizzaStore nyPizzaStore = new NYStylePizzaStore();
        PizzaStore chicagoPizzaStore = new NYStylePizzaStore();

        Pizza nyMargarita = nyPizzaStore.orderPizza(PizzaType.MARGARITA);
        System.out.println("Michael ordered pizza: " + nyMargarita);

        Pizza chicagoQuatroFormaggi = chicagoPizzaStore.orderPizza(PizzaType.QUATRO_FORMAGGI);
        System.out.println("Collette ordered pizza: " + chicagoQuatroFormaggi);

    }
}
