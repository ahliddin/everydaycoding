package tj.durud.hfdp.factory.factorymethod.store;

import tj.durud.hfdp.factory.factorymethod.pizza.Pizza;
import tj.durud.hfdp.factory.factorymethod.pizza.PizzaType;

public abstract class PizzaStore {

    public Pizza orderPizza(PizzaType pizzaType) {

        Pizza pizza = createPizza(pizzaType);
        pizza.prepareDough();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    /**
     * Factory Method
     */
    protected abstract Pizza createPizza(PizzaType pizzaName);
}
