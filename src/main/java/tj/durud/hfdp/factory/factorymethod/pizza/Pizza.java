package tj.durud.hfdp.factory.factorymethod.pizza;

public abstract class Pizza {
    private String pizzaStyle;
    private PizzaType pizzaType;


    protected Pizza(String pizzaStyle, PizzaType pizzaType) {
        this.pizzaStyle = pizzaStyle;
        this.pizzaType = pizzaType;
    }

    public void prepareDough() {
        System.out.println("Preparing dough");
    }

    public void bake() {
        System.out.println("Baking");
    }

    public void cut() {
        System.out.println("Cutting");
    }

    public void box() {
        System.out.println("Boxing pizza");
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "pizzaStyle='" + pizzaStyle + '\'' +
                ", pizzaType=" + pizzaType +
                '}';
    }
}
