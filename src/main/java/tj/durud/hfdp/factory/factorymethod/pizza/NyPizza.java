package tj.durud.hfdp.factory.factorymethod.pizza;

public class NyPizza extends Pizza {

    public NyPizza(String pizzaStyle, PizzaType pizzaType) {
        super(pizzaStyle, pizzaType);
    }
}
