package tj.durud.hfdp.factory.factorymethod.pizza;

public enum PizzaType {
    QUATRO_FORMAGGI,
    MARGARITA
}
