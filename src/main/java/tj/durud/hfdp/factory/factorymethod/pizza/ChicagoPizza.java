package tj.durud.hfdp.factory.factorymethod.pizza;

public class ChicagoPizza extends Pizza {
    public ChicagoPizza(String pizzaStyle, PizzaType pizzaType) {
        super(pizzaStyle, pizzaType);
    }
}
