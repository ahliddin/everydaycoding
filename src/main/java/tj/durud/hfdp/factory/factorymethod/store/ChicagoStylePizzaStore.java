package tj.durud.hfdp.factory.factorymethod.store;

import tj.durud.hfdp.factory.factorymethod.pizza.ChicagoPizza;
import tj.durud.hfdp.factory.factorymethod.pizza.Pizza;
import tj.durud.hfdp.factory.factorymethod.pizza.PizzaType;

public class ChicagoStylePizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(PizzaType type) {
        return new ChicagoPizza("Chicago Style Pizza", type);
    }
}
