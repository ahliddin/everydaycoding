package tj.durud.hfdp.factory.factorymethod.store;

import tj.durud.hfdp.factory.factorymethod.pizza.NyPizza;
import tj.durud.hfdp.factory.factorymethod.pizza.Pizza;
import tj.durud.hfdp.factory.factorymethod.pizza.PizzaType;

public class NYStylePizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(PizzaType type) {

        return new NyPizza("NY Style Pizza", type);

    }
}
