package tj.durud.hfdp.strategy;


/**
 * Gang of Four definition:
 * Define a family of algorithms, encapsulate each one, and make them interchangeable.
 * Strategy lets the algorithm vary independently from the clients that use it.
 */
public class StrategyDemo {

    public static void main(String[] args) {

        doSomeCoding(new JuniorProgrammerStrategy());

        doSomeCoding(new SeniorProgrammerStrategy());

    }

    private static void doSomeCoding(ProgrammerStrategy programmerStrategy) {
        programmerStrategy.code();
    }
}
