package tj.durud.hfdp.strategy;

public interface ProgrammerStrategy {

    void code();
}
