package tj.durud.hfdp.remoteproxy;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by ahliddin on 16/01/2018.
 */
public interface LocalRemote extends Remote {
    String getName() throws RemoteException;

    String getEnv() throws RemoteException;
}
