package tj.durud.hfdp.remoteproxy;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


/**
 * This class should be executed on the remote server while having 'rmiregistry' command executed on the background
 * Once this class is executed then the client can connect to it and call its methods via {@link LocalRemote}
 */
public class LocalRemoteImpl extends UnicastRemoteObject implements LocalRemote {

    private static final long serialVersionUID = 1L;

    public LocalRemoteImpl() throws RemoteException {

    }

    @Override
    public String getName() {
        return "here's my remote proxy";
    }

    @Override
    public String getEnv() {
        return System.getenv().toString();
    }

    public static void main(String[] args) {
        try {
            LocalRemote service = new LocalRemoteImpl();
            Naming.rebind("localhost", service);

        } catch (RemoteException e) {
            e.printStackTrace();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


}
