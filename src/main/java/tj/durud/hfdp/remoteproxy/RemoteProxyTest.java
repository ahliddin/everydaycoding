package tj.durud.hfdp.remoteproxy;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Client class to connect to rmi server and call its method via {@link LocalRemote}
 */

public class RemoteProxyTest {


    public static void main(String... args) {
        try {

            System.getenv();
            LocalRemote service = (LocalRemote) Naming.lookup("rmi://durud.tj/LocalRemoteProxy");

            System.out.println("Calling the service: " + service.getEnv());

        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
