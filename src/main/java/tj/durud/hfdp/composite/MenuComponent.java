package tj.durud.hfdp.composite;

public abstract class MenuComponent {

    public void addMenu(MenuComponent menu) {
        throw new UnsupportedOperationException();
    }

    public void removeMenu(MenuComponent menu) {
        throw new UnsupportedOperationException();
    }

    public String getDescription() {
        throw new UnsupportedOperationException();
    }

    public double getPrice() {
        throw new UnsupportedOperationException();
    }

    public void print() {
        throw new UnsupportedOperationException();
    }


}
