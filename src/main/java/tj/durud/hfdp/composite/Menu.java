package tj.durud.hfdp.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Menu Composite
 */
public class Menu extends MenuComponent {

    private List<MenuComponent> menuItems = new ArrayList<>();

    private String description;

    public Menu(String description) {
        this.description = description;
    }

    @Override
    public void addMenu(MenuComponent menu) {
        menuItems.add(menu);
    }

    @Override
    public void removeMenu(MenuComponent menu) {
        menuItems.remove(menu);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void print() {

        System.out.println("++++++++++++++++++++");
        System.out.println(description);

        for (MenuComponent menuItem : menuItems) {
            menuItem.print();
        }
    }
}
