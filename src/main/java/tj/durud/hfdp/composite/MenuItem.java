package tj.durud.hfdp.composite;

public class MenuItem extends MenuComponent {

    private String description;
    private double price;

    public MenuItem(String description, double price) {
        this.description = description;
        this.price = price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void print() {
        System.out.println("---");
        System.out.println(description + " - " + price + ",- czk");
    }

}
