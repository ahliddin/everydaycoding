package tj.durud.hfdp.composite;


/**
 * Gang of Four definition:
 * Compose objects into tree structures to represent part-whole hierarchies.
 * Composite lets clients treat individual objects and compositions of objects uniformly.
 * <p>
 * Explanation: A Composite contains components. Components can be other composites or leaf elements. It is recursive.
 */
public class CompositeDemo {

    public static void main(String[] args) {

        MenuItem kebab = new MenuItem("Lamb kebab", 290);
        MenuItem cheeseCake = new MenuItem("Cheesecake", 60);
        MenuItem coke = new MenuItem("Coca Cola", 40);
        MenuItem omelet = new MenuItem("Omelet", 140);

        Menu breakfast = new Menu("BREAKFAST MENU");
        Menu dinner = new Menu("DINNER MENU");
        Menu dessert = new Menu("DESSERT MENU");

        breakfast.addMenu(omelet);
        dessert.addMenu(cheeseCake);

        dinner.addMenu(kebab);
        dinner.addMenu(coke);
        dinner.addMenu(dessert);

        Menu allMenus = new Menu("ALL MENUS");
        allMenus.addMenu(breakfast);
        allMenus.addMenu(dinner);

        allMenus.print();
        System.out.println("//////////////////////");
        dinner.print();
        System.out.println("//////////////////////");
        breakfast.print();
    }
}
