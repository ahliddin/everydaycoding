package tj.durud.hfdp.facade;

public class CPU {

    public void freeze() {
        System.out.println("CPU freeze");
    }

    public void jump(long address) {
        System.out.println("CPU jumping to address: " + address);
    }

    public void execute() {
        System.out.println("CPU executing");
    }
}
