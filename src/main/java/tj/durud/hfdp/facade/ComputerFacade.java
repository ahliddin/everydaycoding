package tj.durud.hfdp.facade;

public class ComputerFacade {

    private static final int BOOT_ADDRESS = 0b1010;
    private HardDrive hardDrive;
    private CPU cpu;
    private RAM ram;

    public ComputerFacade() {
        this.hardDrive = new HardDrive();
        this.cpu = new CPU();
        this.ram = new RAM();
    }

    public void start() {

        cpu.freeze();

        ram.load(BOOT_ADDRESS, hardDrive.read());

        cpu.jump(BOOT_ADDRESS);

        cpu.execute();

    }
}
