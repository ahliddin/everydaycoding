package tj.durud.hfdp.facade;

public class RAM {

    public void load(int position, byte[] data) {

        System.out.println("RAM loading at the possiton [" + position + "] the following data: " + data);

    }
}
