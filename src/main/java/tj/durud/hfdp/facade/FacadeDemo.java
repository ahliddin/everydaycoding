package tj.durud.hfdp.facade;


/**
 * Gang of Four definition:
 * Provide a unified interface to a set of interfaces in a subsystem.
 * Facade defines a higher-level interface that makes the subsystem easier to use.
 * <p>
 * <p>
 * Difference from Adapter Pattern:
 * It's in the intent - the Adapter intents to <i>alter</i> an interface so that it matches what one a client is expecting;
 * The intent of the Facade Pattern is to provide a <i>simplified</i> interface to a subsystem
 * and client can still use the underlying interfaces if needs to.
 */
public class FacadeDemo {

    public static void main(String[] args) {
        ComputerFacade computerFacade = new ComputerFacade();

        computerFacade.start();
    }

}
