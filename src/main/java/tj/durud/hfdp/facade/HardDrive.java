package tj.durud.hfdp.facade;

public class HardDrive {

    public byte[] read() {
        System.out.println("HardDrive read() is returning bytes array of the read data");
        return "read data".getBytes();
    }
}
