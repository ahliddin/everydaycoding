package tj.durud.hfdp.iterator;


import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

/**
 * Gang of Four definition:
 * The Iterator Pattern provides a way to access elements of an aggregate object sequentially
 * without exposing its underlying representation.
 */
public class IteratorDemo {

    public static void main(String[] args) {
        Family family = new Family();
        family.build();

        Iterator<FamilyMember> iterator = family.getIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("------------------");

        // In order to loop through for in, the Family needs to implement Iterable
        for (FamilyMember member : family) {
            System.out.println(member);
        }
    }


}

class FamilyIterator implements Iterator<FamilyMember> {

    private FamilyMember[] members;
    private int position = 0;


    public FamilyIterator(FamilyMember[] members) {
        this.members = members;
    }

    @Override
    public boolean hasNext() {
        return position < members.length && members[position] != null;
    }

    @Override
    public FamilyMember next() {
        return members[position++];
    }


}


class Family implements Iterable<FamilyMember> {
    private static final int MAX_MEMBERS = 6;
    private FamilyMember[] members = new FamilyMember[MAX_MEMBERS];

    public void build() {
        members[0] = new FamilyMember("Bob", "father");
        members[1] = new FamilyMember("Ann", "mother");
        members[2] = new FamilyMember("Charlie", "oldest son");
        members[3] = new FamilyMember("Edward", "youngest son");
        members[4] = new FamilyMember("Helen", "only daughter");
    }

    public FamilyIterator getIterator() {
        return new FamilyIterator(members);
    }

    @NotNull
    @Override
    public Iterator<FamilyMember> iterator() {
        return new FamilyIterator(members);
    }
}

class FamilyMember {
    private String name;
    private String relation;

    public FamilyMember(String name, String relation) {
        this.name = name;
        this.relation = relation;
    }

    @Override
    public String toString() {
        return "FamilyMember{" +
                "name='" + name + '\'' +
                ", relation='" + relation + '\'' +
                '}';
    }
}