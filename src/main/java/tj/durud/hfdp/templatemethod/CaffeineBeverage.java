package tj.durud.hfdp.templatemethod;

public abstract class CaffeineBeverage {

    public final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
    }

    private void pourInCup() {
        System.out.println("Pouring " + drinkName() + " in cup");
    }

    protected abstract void brew();

    private void boilWater() {
        System.out.println("Boiling water for " + drinkName());

    }

    protected abstract String drinkName();

    protected abstract void addCondiments();
}
