package tj.durud.hfdp.templatemethod;


/**
 * Define the skeleton of an algorithm in an operation, deferring some steps to subclasses.
 * Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.
 */
public class TemplateMethodDemo {

    public static void main(String[] args) {
        CaffeineBeverage tea = new Tea();
        CaffeineBeverage coffee = new Coffee();

        tea.prepareRecipe();
        System.out.println("-----------------");
        coffee.prepareRecipe();
    }

}
