package tj.durud.hfdp.templatemethod;

public class Tea extends CaffeineBeverage {
    @Override
    protected void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    protected String drinkName() {
        return "tea";
    }

    @Override
    protected void addCondiments() {
        System.out.println("Adding some lemon");

    }
}
