package tj.durud.hfdp.templatemethod;

public class Coffee extends CaffeineBeverage {


    @Override
    protected void brew() {
        System.out.println("Dripping coffee through filter");
    }

    @Override
    protected void addCondiments() {
        System.out.println("Adding some milk into coffee");

    }

    @Override
    protected String drinkName() {
        return "coffee";
    }
}
