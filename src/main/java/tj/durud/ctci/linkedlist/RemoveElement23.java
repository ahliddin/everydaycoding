package tj.durud.ctci.linkedlist;


import tj.durud.ctci.linkedlist.linkedlist.Node;
import tj.durud.ctci.linkedlist.linkedlist.SinglyLinkedList;

import java.util.Objects;

import static tj.durud.ctci.linkedlist.linkedlist.LinkedListUtils.generateList;

/**
 * Implement an algorithm to delete a node from the middle of a singly linked list given access only to that node.
 */
public class RemoveElement23 {


    public static void main(String[] args) {

        SinglyLinkedList list = generateList(11);

        list.printList(true);

        Node middleNode = getMiddleNode(list);
        System.out.println("Middle node data: " + middleNode.getData());

        removeNodeOptionOne(middleNode);

        list.printList(true);
    }


    private static Node getMiddleNode(SinglyLinkedList list) {

        Node node = list.getHead();

        for (int i = 0; i < list.getSize(); i++) {
            if (i == list.getSize() / 2) {
                break;
            }
            node = node.getNext();
        }

        return node;
    }

    /**
     * Assign the value of the next node to the to-be-removed node; Assign next-next node as next node;
     * <p>
     * NOTE: only works if the node is not the last one
     */
    private static void removeNodeOptionOne(Node node) {

        Objects.requireNonNull(node);
        Objects.requireNonNull(node.getNext());

        Node nextNode = node.getNext();
        node.setData(nextNode.getData());
        node.setNext(nextNode.getNext());

    }
}
