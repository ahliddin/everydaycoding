package tj.durud.ctci.linkedlist;


import tj.durud.ctci.linkedlist.linkedlist.LinkedList;
import tj.durud.ctci.linkedlist.linkedlist.Node;
import tj.durud.ctci.linkedlist.linkedlist.SinglyLinkedList;

import java.util.Objects;

import static tj.durud.ctci.linkedlist.linkedlist.LinkedListUtils.generateListRandomly;

/**
 * Partition a linked list around value X such that all values less than X come before all values equal or greater than X.
 */
public class PartitionLinkedList24 {


    public static void main(String[] args) {

        int pivot = 3;
        LinkedList list1 = generateListRandomly(11, 10);
        SinglyLinkedList list2 = copyList(list1);
        SinglyLinkedList list3 = copyList(list1);


        System.out.println("-------------------------\nList 1:");
        list1.printList(true);
        partitionLinkedListOptionOne(list1, pivot);
        System.out.println("\nList 1 partitioned:");
        list1.printList(true);

        System.out.println("-------------------------\nList 2:");
        list2.printList(true);
        partitionLinkedListOptionTwo(list2, pivot);
        System.out.println("List 2 partitioned:");
        list2.printList(true);
        System.out.println("List 2:");

        System.out.println("-------------------------\nList 3:");
        list3.printList(true);
        partitionLinkedListOptionTwoB(list3, pivot);
        System.out.println("List 3 partitioned:");
        list3.printList(true);

    }

    private static SinglyLinkedList copyList(LinkedList list) {

        SinglyLinkedList copyOfList = new SinglyLinkedList();
        Node node = list.getHead();
        while (node != null) {
            copyOfList.addNode(new Node(node.getData()));
            node = node.getNext();
        }

        return copyOfList;
    }

    /**
     * We iterate through linked list and nodes with value >= than X we extract and put to the end of the queue;
     * To avoid the infinite loop we tag replaces nodes as visited.
     * Note: this solution requires double-ended link list
     */
    private static void partitionLinkedListOptionOne(LinkedList list, int pivot) {
        Objects.requireNonNull(list.getHead());
        Objects.requireNonNull(list.getHead().getNext());

        Node head = list.getHead();
        Node tail = list.getTail();
        Node runner = head;


        while (runner != null && !runner.isVisited()) {
            if (runner.getData() >= pivot) {
                if (runner == head) {
                    head = head.getNext();
                    head.setPrev(null);
                    list.setHead(head);

                } else {
                    runner.getPrev().setNext(runner.getNext());
                    runner.getNext().setPrev(runner.getPrev());
                }

                Node tmp = tail;
                tail.setNext(runner);
                tail = runner;

                runner = runner.getNext();
                tail.setNext(null);
                tail.setPrev(tmp);
                tail.setVisited(true);

            } else {
                runner = runner.getNext();
            }
        }
    }

    /**
     * We iterate through the linked list and create two separate lists with values less than X and more or equal to X;
     * Then we merge both;
     */
    private static void partitionLinkedListOptionTwo(SinglyLinkedList list, int pivot) {

        Node beforeStart = null;
        Node beforeEnd = null;
        Node afterStart = null;
        Node afterEnd = null;

        Node node = list.getHead();

        while (node != null) {
            Node next = node.getNext();
            node.setNext(null);

            if (node.getData() < pivot) {
                if (beforeStart == null) {
                    beforeStart = node;
                    beforeEnd = node;
                } else {
                    beforeEnd.setNext(node);
                    beforeEnd = node;
                }

            } else {
                if (afterStart == null) {
                    afterStart = node;
                    afterEnd = node;
                } else {
                    afterEnd.setNext(node);
                    afterEnd = node;
                }
            }

            node = next;
        }

        if (beforeStart == null) {
            list.setHead(afterStart);
            list.setTail(afterEnd);
            return;
        }

        beforeEnd.setNext(afterStart);
        list.setHead(beforeStart);
        list.setTail(afterEnd);
    }


    /**
     * Similar to OptionTwo, but instead of using two extra variables to track the tail of the two parts, be just put the
     * nodes in front of each partition.
     */
    private static void partitionLinkedListOptionTwoB(SinglyLinkedList list, int pivot) {

        Node beforeStart = null;
        Node afterStart = null;

        Node node = list.getHead();

        while (node != null) {
            Node next = node.getNext();

            if (node.getData() < pivot) {
                node.setNext(beforeStart);
                beforeStart = node;

            } else {
                node.setNext(afterStart);
                afterStart = node;
            }
            node = next;
        }

        if (beforeStart == null) {
            list.setHead(afterStart);
            list.setTail(getTail(afterStart));
            return;
        }

        Node beforeEnd = getTail(beforeStart);
        Node afterEnd = getTail(afterStart);

        beforeEnd.setNext(afterStart);

        list.setHead(beforeStart);
        list.setTail(afterEnd);

    }

    private static Node getTail(Node node) {
        while (node.getNext() != null) {
            node = node.getNext();
        }
        return node;
    }


}
