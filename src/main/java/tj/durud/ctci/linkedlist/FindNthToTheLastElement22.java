package tj.durud.ctci.linkedlist;


import tj.durud.ctci.linkedlist.linkedlist.Node;
import tj.durud.ctci.linkedlist.linkedlist.SinglyLinkedList;

import java.util.Random;

/**
 * Find Nth to the last element in the singly linked list
 */
public class FindNthToTheLastElement22 {

    public static void main(String[] args) {

        test(10, 1);
        test(10, 2);
        test(10, 5);

        test(-10, 5);

        test(100, 100);


        System.out.println("Test successful");
    }

    private static void test(int size, int index) {

        if (size < 0 || index < 0) {
            System.out.println("INVALID INPUT VALUES");
            System.out.println("-------------");
            return;
        }

        SinglyLinkedList linkedList = new SinglyLinkedList();

        int expected = enrichLinkedList(linkedList, size, index);

        linkedList.printList(size < 200);

        Node node = findElementFromTailOptionTwo(linkedList, index);

        System.out.println("Found " + -index + "th element: [" + node.getData() + "]");
        System.out.println("-------------");

        if (node.getData() != expected) {
            throw new RuntimeException("Incorrect value, expected: " + expected);
        }
    }

    /**
     * Have a node which will start moving only when there's a difference of 'index' number of elements between the first runner
     */
    private static Node findElementFromTailOptionOne(SinglyLinkedList list, int index) {

        Node searchedNode = list.getHead();
        Node runner = list.getHead();

        int counter = 0;

        while (runner != null) {
            runner = runner.getNext();

            if (counter >= index) {
                searchedNode = searchedNode.getNext();
            }

            counter++;
        }


        return searchedNode;
    }

    private static Node findElementFromTailOptionTwo(SinglyLinkedList list, int index) {

        Node[] result = new Node[1];

        recursiveSolution(list.getHead(), index, result);
        Node searchedNode = result[0];


        return searchedNode;
    }

    private static int recursiveSolution(Node node, int index, Node[] result) {
        if (node == null) {
            return 0;
        }

        int k = recursiveSolution(node.getNext(), index, result) + 1;

        if (k == index) {
            result[0] = node;
        }

        return k;

    }




    private static int enrichLinkedList(SinglyLinkedList list, int size, int index) {

        int value = Integer.MIN_VALUE;
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            int data = random.nextInt(100);
            list.addNode(new Node(data));

            if (i == size - index) {
                value = data;
            }
        }

        return value;
    }


}
