package tj.durud.ctci.linkedlist;


import tj.durud.ctci.linkedlist.linkedlist.Node;
import tj.durud.ctci.linkedlist.linkedlist.SinglyLinkedList;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Remove duplicates from an unsorted linked list.
 * How would you solve it if a temporary buffer is not allowed?
 */
public class RemoveDuplicates21 {


    public static void main(String[] args) {

        test(10, 10, true);
        test(200, 100, false);
        test(2000, 1000, false);
        test(50000, 10000, false);

    }

    private static void test(int nodes, int valueRange, boolean printElements) {

        Random random = new Random();
        SinglyLinkedList listOne = new SinglyLinkedList();
        SinglyLinkedList listTwo = new SinglyLinkedList();
        Set<Integer> controlSet = new HashSet<>();


        for (int i = 0; i < nodes; i++) {
            int value = random.nextInt() % valueRange;

            listOne.addNode(new Node(value));
            listTwo.addNode(new Node(value));

            controlSet.add(value);
        }

        listOne.printList(printElements);
        removeDuplicatesOptionOne(listOne.getHead());
        listOne.printList(printElements);

        listTwo.printList(printElements);
        removeDuplicatesOptionTwo(listTwo.getHead());
        listTwo.printList(printElements);


        assertHaveSameElements(controlSet, listOne);
        assertHaveSameElements(controlSet, listTwo);
    }

    /**
     * Use set to check if the element is already present, if yes then eliminate it
     */
    private static void removeDuplicatesOptionOne(Node currentNode) {
        Set<Integer> nodeValues = new HashSet<>();


        Node prevNode = currentNode;
        while (currentNode != null) {
            if (nodeValues.contains(currentNode.getData())) {
                prevNode.setNext(currentNode.getNext());

            } else {
                nodeValues.add(currentNode.getData());
                prevNode = currentNode;
            }
            currentNode = currentNode.getNext();
        }
    }

    /**
     * Removing duplicate elements without additional data structure place, but with quadratic time complexity
     */
    private static void removeDuplicatesOptionTwo(Node currentNode) {

        while (currentNode != null) {
            Node runner = currentNode;

            while (runner.getNext() != null) {
                if (runner.getNext().getData() == currentNode.getData()) {
                    runner.setNext(runner.getNext().getNext());

                } else {
                    runner = runner.getNext();
                }

            }
            currentNode = currentNode.getNext();
        }

    }

    private static void assertHaveSameElements(Set<Integer> testSet, SinglyLinkedList list) {
        Node node = list.getHead();

        assert testSet.size() == list.getSize();

        while (node != null) {
            if (!testSet.contains(node.getData())) {
                throw new AssertionError("Node data " + node.getData() + " is not in testSet");
            }
            node = node.getNext();
        }

        System.out.println("Duplicates were removed correctly");
    }
}


