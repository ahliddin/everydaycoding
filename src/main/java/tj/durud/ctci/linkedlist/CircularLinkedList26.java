package tj.durud.ctci.linkedlist;


import tj.durud.ctci.linkedlist.linkedlist.LinkedListUtils;
import tj.durud.ctci.linkedlist.linkedlist.Node;
import tj.durud.ctci.linkedlist.linkedlist.SinglyLinkedList;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Given a circular linked list implement an algorithm that will return a node at the beginning of the loop.
 * Example: a -> b -> c -> d -> e -> f -> c - should return 'c' node
 */

public class CircularLinkedList26 {

    public static void main(String[] args) {

        SinglyLinkedList circularList = LinkedListUtils.generateList(5);
        circularList.printList(true);

        Node loopStart = circularList.getHead().getNext();
        circularList.getTail().setNext(loopStart);
        Node result = findFirstNodeOptionTwo(circularList);
        validate(result, loopStart);


        loopStart = circularList.getHead().getNext().getNext();
        circularList.getTail().setNext(loopStart);
        result = findFirstNodeOptionTwo(circularList);
        validate(result, loopStart);

        loopStart = circularList.getTail();
        circularList.getTail().setNext(loopStart);
        result = findFirstNodeOptionTwo(circularList);
        validate(result, loopStart);

    }

    private static void validate(Node result, Node expected) {
        Objects.requireNonNull(result);

        if (result != expected) {
            System.out.println("Incorrect result! Expected [" + expected.getData() + "], Actual [" + result.getData() + "]");
        } else {
            System.out.println("Correct result [" + result.getData() + "]");
        }
    }

    /**
     * Naive solution - using set
     */
    private static Node findFirstNodeOptionOne(SinglyLinkedList circularList) {
        Set<Integer> set = new HashSet<>();
        Node node = circularList.getHead();

        while (node != null) {
            if (!set.contains(node.getData())) {
                set.add(node.getData());
            } else {
                return node;
            }
            node = node.getNext();
        }

        return null;
    }


    /**
     * Runner1 and runner2 will collide mod(K, LOOP_LENGTH) nodes before the start of the loop,
     * where K is the length of the list before the start of the loop.
     */
    private static Node findFirstNodeOptionTwo(SinglyLinkedList circularList) {
        Node head = circularList.getHead();
        Node runner1 = head.getNext();
        Node runner2 = head.getNext().getNext();

        while (runner1 != runner2) {
            runner1 = runner1.getNext();
            runner2 = runner2.getNext().getNext();
        }

        runner1 = head;
        while (runner1 != runner2) {
            runner1 = runner1.getNext();
            runner2 = runner2.getNext();
        }

        return runner2;


    }
}





