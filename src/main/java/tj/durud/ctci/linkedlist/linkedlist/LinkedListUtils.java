package tj.durud.ctci.linkedlist.linkedlist;

import java.util.Random;

public class LinkedListUtils {

    public static SinglyLinkedList generateList(int size) {
        SinglyLinkedList list = new SinglyLinkedList();

        for (int i = 0; i < size; i++) {
            Node node = new Node(i);
            list.addNode(node);
        }

        return list;
    }

    public static LinkedList generateListRandomly(int size, int max) {
        LinkedList list = new LinkedList();

        Random random = new Random();

        for (int i = 0; i < size; i++) {
            int val = random.nextInt(max);
            Node node = new Node(val);
            list.addNode(node);
        }

        return list;
    }

}
