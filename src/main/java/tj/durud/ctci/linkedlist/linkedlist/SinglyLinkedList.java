package tj.durud.ctci.linkedlist.linkedlist;

public class SinglyLinkedList {
    protected Node head;
    protected Node tail;

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public int getSize() {
        Node n = head;
        int size = 0;
        while (n != null) {
            size++;
            n = n.getNext();
        }
        return size;
    }

    public void addNode(Node node) {
        if (head == null) {
            head = node;
            tail = node;

        } else {
            tail.setNext(node);
            tail = tail.getNext();
        }
    }

    public void printList(boolean printElements) {
        Node n = head;
        StringBuilder sb = new StringBuilder();

        while (printElements && n != null) {
            sb.append("[")
                    .append(n.getData())
                    .append("]");

            if (n.getNext() != null) {
                sb.append(" -> ");
            }
            n = n.getNext();
        }

        System.out.println("Size: " + getSize() + ": " + sb);
    }

    public void printListRecursive(boolean printElements) {
        Node n = tail;
        StringBuilder sb = new StringBuilder();

        while (printElements && n != null) {
            sb.append("[")
                    .append(n.getData())
                    .append("]");

            if (n.getPrev() != null) {
                sb.append(" -> ");
            }
            n = n.getPrev();
        }

        System.out.println("Size: " + getSize() + ": " + sb);
    }
}
