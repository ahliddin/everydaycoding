package tj.durud.ctci.linkedlist.linkedlist;

public class LinkedList extends SinglyLinkedList {

    @Override
    public void addNode(Node node) {
        if (head == null) {
            head = node;
            tail = node;

        } else {
            Node tmp = tail;
            tail.setNext(node);
            tail = tail.getNext();
            tail.setPrev(tmp);
        }
    }

}
