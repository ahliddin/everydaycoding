package tj.durud.ctci.arrayandstring;


import java.util.Arrays;

/**
 * Given a matrix NxN, write a method to rotate the matrix to 90 degrees.
 * Can you do it on place?
 */

public class RotateMatrix16 {

    public static void main(String... args) {

        testWithMatrix(1);
        testWithMatrix(2);
        testWithMatrix(3);
        testWithMatrix(4);
        testWithMatrix(5);
        testWithMatrix(6);

    }

    private static void testWithMatrix(int size) {

        int[][] matrix = generateMatrix(size);

        print(matrix);

        int[][] rotatedMatrix = rotateOptionTwo(matrix);

        print(rotatedMatrix);
    }


    /**
     * Declare the matrix of the same size and copy over the columns and rows
     */
    private static int[][] rotateOptionOne(int[][] matrix) {

        assert matrix.length == matrix[0].length; // it should be NxN matrix

        int n = matrix.length;
        int[][] rMatrix = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                rMatrix[j][n - 1 - i] = matrix[i][j];

            }
        }


        return rMatrix;
    }


    /**
     * Do the same in place
     */
    private static int[][] rotateOptionTwo(int[][] matrix) {
        assert matrix.length == matrix[0].length; // it should be NxN matrix

        int len = matrix.length - 1;

        for (int layer = 0; layer < matrix.length / 2; layer++) {

            for (int i = layer; i < len - layer; i++) {

                //  int left = matrix[len-i][layer];
                //  int top = matrix[layer][i];
                //  int right = matrix[i][len-layer];
                //  int bottom = matrix[len-layer][len-i];

                int tmp = matrix[len - i][layer]; // left
                matrix[len - i][layer] = matrix[len - layer][len - i]; // left = bottom
                matrix[len - layer][len - i] = matrix[i][len - layer]; // bottom = right
                matrix[i][len - layer] = matrix[layer][i]; // right = top
                matrix[layer][i] = tmp;


            }

        }


        return matrix;
    }

    private static void print(int[][] matrix) {

        System.out.println("++++++++++++++++");

        for (int[] array : matrix) {
            System.out.println(Arrays.toString(array));
        }

        System.out.println("----------------");

    }

    private static int[][] generateMatrix(int n) {

        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = Integer.parseInt("" + i + j);
            }
        }

        return matrix;
    }
}
