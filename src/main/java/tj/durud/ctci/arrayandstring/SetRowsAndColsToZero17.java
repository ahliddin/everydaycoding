package tj.durud.ctci.arrayandstring;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Given matrix NxM if the element in matrix is 0, then set its entire row and column to zero
 */
public class SetRowsAndColsToZero17 {

    public static void main(String[] args) {

        Set<Tuple> zeroElems = new HashSet<>();

        int[][] matrix = generateMatrix(4, 4);


        printMatrix(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) {
                    zeroElems.add(new Tuple(i, j));
                }
            }
        }

        zeroUpRowsAndCols(matrix, zeroElems);

        printMatrix(matrix);

        System.out.println(zeroElems);
    }

    private static void zeroUpRowsAndCols(int[][] matrix, Set<Tuple> zeroElems) {

        for (Tuple zeroElem : zeroElems) {

            for (int i = 0; i < matrix[0].length; i++) {
                matrix[zeroElem.row][i] = 0;
            }

            for (int i = 0; i < matrix.length; i++) {
                matrix[i][zeroElem.col] = 0;
            }
        }


    }


    private static int[][] generateMatrix(int row, int col) {

        int[][] matrix = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = (i + j) % 5;
            }
        }

        return matrix;
    }


    private static void printMatrix(int[][] matrix) {
        System.out.println("--------------");
        for (int[] array : matrix) {
            System.out.println(Arrays.toString(array));
        }

    }

    private static class Tuple {
        private int row, col;

        Tuple(int row, int col) {
            this.row = row;
            this.col = col;
        }


        @Override
        public String toString() {
            return "{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }


}
