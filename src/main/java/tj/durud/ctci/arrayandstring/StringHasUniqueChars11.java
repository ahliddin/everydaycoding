package tj.durud.ctci.arrayandstring;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Implement an algorithm to determine if the string has all unique characters.
 * What if you can't use additional data structures.
 */
public class StringHasUniqueChars11 {

    public static void main(String[] args) {

        String str = "aasdfqwe";

        System.out.println(optionOne(str));
        System.out.println(optionTwo(str));
    }

    /**
     * Implementation using Set
     */
    private static boolean optionOne(String str) {

        Set<Character> set = new HashSet<>();


        for (Character c : str.toCharArray()) {
            if (set.contains(c)) {
                return false;
            }
            set.add(c);

        }

        return true;
    }

    /**
     * Not using additional data structure - sorting and afterwards checking two consecutive chars
     */
    private static boolean optionTwo(String str) {
        char[] chars = str.toCharArray();
        Arrays.sort(chars);

        for (int i = 1; i < chars.length; i++) {
            if (chars[i] == chars[i - 1]) {
                return false;
            }
        }

        return true;
    }


}
