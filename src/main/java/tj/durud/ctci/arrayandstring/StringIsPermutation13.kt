package tj.durud.ctci.arrayandstring


/**
 * Given two strings, define whether one string is a permutation of another
 *
 * Permutation - contains the same chars but in any other order
 */
fun main(args: Array<String>) {

    val str1 = "some string"
    val str2 = "string somm"

    println(optionOne(str1, str2))

    println(optionOne("asdfadsf", "asdfasdf"))


}

fun optionOne(str: String, perm: String): Boolean {

    val charCounter = HashMap<Char, Int>()

    for (c in str.toCharArray()) {
        var counter: Int? = charCounter.get(c)

        if (counter == null) {
            counter = 0
        }

        charCounter.put(c, counter.inc())
    }

    for (c in perm.toCharArray()) {

        var counter = charCounter.get(c)
        if (counter == null || --counter < 0) {
            println("counter == null or negative: " + counter)
            return false
        }

        charCounter.put(c, counter)

    }

    for (counter in charCounter.values) {
        if (counter > 0) {
            return false
        }
    }


    return true
}

fun optionTwo(str1: String, str2: String): Boolean {

    return false
}