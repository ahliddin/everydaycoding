package tj.durud.ctci.stacksandqueues;


/**
 * How would you implement a stack that would have in addition to push and pop,
 * the method min that would return the minimum element.
 * All operations should be in O(1)
 */
public class StackMin32 {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();

        for (int i = 10; i > 0; i--) {
            stack.push(i);
        }

        while (true) {
            System.out.println(stack.min());
            if (stack.pop() == null) {
                break;
            }
        }

    }

}

