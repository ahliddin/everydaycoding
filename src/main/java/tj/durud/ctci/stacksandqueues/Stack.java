package tj.durud.ctci.stacksandqueues;

public class Stack<T extends Comparable<T>> {

    private T element;
    private Node top;

    public void push(T e) {
        if (top == null) {
            top = new Node(e, e);
            return;

        }
        T min = e.compareTo(top.min) < 0 ? e : top.min;

        Node tmp = new Node(e, min);
        tmp.next = top;
        top = tmp;

    }


    public T pop() {
        if (top == null) {
            return null;
        }

        T res = top.data;
        top = top.next;

        return res;
    }

    public T min() {
        if (top == null) {
            System.out.println("Stack is empty!!");
            return null;
        }
        return top.min;
    }

    private class Node {
        T data;
        T min;
        Node next;
        Node prev; // used by Min list


        private Node(T data, T min) {
            this.data = data;
            this.min = min;
        }


    }

}


