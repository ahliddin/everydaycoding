OCP - Java 8

Chapter 1
* Static methods cannot be overridden, but only hidden. Static method being hidden by the subclass
  still has to have a correct access permissions otherwise a compilation error.
*
