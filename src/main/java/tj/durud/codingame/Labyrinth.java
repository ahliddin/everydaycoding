package tj.durud.codingame;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.abs;

public class Labyrinth {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int R = in.nextInt(); // number of rows.
        int C = in.nextInt(); // number of columns.
        int A = in.nextInt(); // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
        char[][] maze = new char[R][C];


        Set<Cell> visited = new HashSet<>();
        Stack<Cell> prevs = new Stack<>();

        // game loop
        while (true) {
            int KR = in.nextInt(); // row where Kirk is located.
            int KC = in.nextInt(); // column where Kirk is located.
            for (int i = 0; i < R; i++) {
                maze[i] = in.next().toCharArray();
            }

            Cell current = new Cell(KR, KC, maze[KR][KC]);
            visited.add(current);

            if (findCell(maze, '?') == null || (prevs.empty() && getUnvisitedNeighbors(maze, current, visited).size() == 0)) {
                print(maze);
                Cell controlRoom = findCell(maze, 'C');
                Stack<Cell> path = findShortestPath(maze, current, controlRoom);
                while (path.size() > 2) {
                    move(path.pop(), path.peek());
                }

                Cell start = findCell(maze, 'T');
                path = findShortestPath(maze, current, start);
                while (path.size() > 2) {
                    move(path.pop(), path.peek());
                }
                return;
            }
            boolean moved = false;
            for (Cell neighbor : getNeighbors(maze, current)) {
                if (!visited.contains(neighbor) && neighbor.val != 'C') { // exploring all cells without stepping into control room
                    prevs.add(current);
                    move(current, neighbor);
                    moved = true;
                    break;
                }
            }
            if (!moved) {
                move(current, prevs.pop());
            }
        }
    }

    private static Stack<Cell> findShortestPath(char[][] maze, Cell from, Cell to) {
        PriorityQueue<Cell> open = new PriorityQueue<>(Comparator.comparingDouble(Cell::fCost));
        List<Cell> closed = new ArrayList<>();

        open.add(from);

        while (!open.isEmpty()) {
            Cell currentCell = open.poll();
            if (currentCell.equals(to)) {
                return getPathStack(currentCell);
            }
            for (Cell neighbor : getNeighbors(maze, currentCell)) {
                neighbor.prev = currentCell;
                neighbor.calculateHCost(to);
                neighbor.calculateGCost();

                if (closed.contains(neighbor)) {
                    int ind = closed.indexOf(neighbor);
                    if (closed.get(ind).fCost() < neighbor.fCost()) {
                        continue;
                    }
                }

                if (open.contains(neighbor)) {
                    Cell existing = null;
                    for (Cell c : open) {
                        if (c.equals(neighbor)) {
                            existing = c;
                        }
                    }
                    if (existing.fCost() > neighbor.fCost()) {
                        open.remove(existing);
                        open.add(neighbor);
                    }
                } else {
                    open.add(neighbor);
                }
            }
            closed.add(currentCell);
        }
        System.err.println("Searched cell is not found");
        throw new RuntimeException();
    }

    private static Stack<Cell> getPathStack(Cell lastCell) {
        Stack<Cell> path = new Stack<>();
        Cell runner = lastCell;
        while (runner != null) {
            path.push(runner);
            runner = runner.prev;
        }
        return path;
    }

    private static void print(char[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            System.err.println(maze[i]);
        }

    }

    private static void move(Cell from, Cell to) {
        if (to.x - from.x == 0) {
            if ((to.y - from.y) == -1) {
                System.out.println("LEFT");
                System.err.println("LEFT");
            } else if ((to.y - from.y) == 1) {
                System.out.println("RIGHT");
                System.err.println("RIGHT");
            }
        } else if (to.y - from.y == 0) {
            if ((to.x - from.x) == -1) {
                System.out.println("UP");
                System.err.println("UP");
            } else if ((to.x - from.x) == 1) {
                System.out.println("DOWN");
                System.err.println("DOWN");
            }
        }
    }

    private static List<Cell> getUnvisitedNeighbors(char[][] maze, Cell cell, Set<Cell> visited) {
        return getNeighbors(maze, cell).stream().filter(c -> !visited.contains(c)).collect(Collectors.toList());


    }

    private static List<Cell> getNeighbors(char[][] maze, Cell cell) {
        int[] xs = {cell.x - 1, cell.x + 1, cell.x, cell.x};
        int[] ys = {cell.y, cell.y, cell.y - 1, cell.y + 1};

        List<Cell> list = new ArrayList<>();

        for (int i = 0; i < ys.length; i++) {
            int x = xs[i];
            int y = ys[i];
            if (validCell(maze, x, y)) {
                list.add(new Cell(x, y, maze[x][y]));
            }
        }

        return list;
    }

    private static boolean validCell(char[][] maze, int x, int y) {
        return x >= 0 && y >= 0 && x < maze.length && y < maze[x].length && maze[x][y] != '#' && maze[x][y] != '?';
    }

    private static Cell findCell(char[][] maze, char s) {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                if (maze[i][j] == s) {
                    return new Cell(i, j, maze[i][j]);
                }
            }
        }
        return null;
    }

    private static class Cell {
        char val;
        int x, y;
        double gCost = 0; // distance from start to here
        double hCost = 0; // distance from here to goal (heuristic)
        Cell prev = null;

        Cell(int x, int y, char val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        public double fCost() {
            return gCost + hCost;
        }

        public void calculateGCost() {
            Objects.requireNonNull(prev);
            gCost = this.prev.gCost + 1;

        }

        public void calculateHCost(Cell goal) {
            this.hCost = Math.sqrt(abs(goal.x - this.x) + abs(goal.y - this.y));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cell cell = (Cell) o;
            return x == cell.x &&
                    y == cell.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

    }

}