package tj.durud.fakeinstagramdetector;

import java.util.Collections;
import java.util.List;

public class Account {
    private Account parent;
    private int level;
    private boolean visited = false;
    private List<Account> followers = Collections.emptyList();

    public Account(Account parent, int level) {
        this.parent = parent;
        this.level = level;
    }

    synchronized public List<Account> getFollowers() {
        if (followers == null || followers.isEmpty()) {
            followers = AccountUtils.generateRandomFollowers(this);
        }
        return followers;
    }


    public Account getParent() {
        return parent;
    }

    public int getLevel() {
        return level;
    }

    public boolean isVisited() {
        return visited;
    }
}