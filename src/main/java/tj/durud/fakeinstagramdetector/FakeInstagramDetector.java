package tj.durud.fakeinstagramdetector;

import java.util.*;

public class FakeInstagramDetector {

    public static void main(String[] args) {
        Account rootAccount = new Account(null, 1);

        Queue<Account> accounts = new ArrayDeque<>();
        accounts.add(rootAccount);

        int level = 2;
        Map<Character, Integer> counts = new HashMap<>();
        while (!accounts.isEmpty()) {
            Account acc = accounts.poll();
            addToCounts(acc.getFollowers().size(), counts);
            if (acc.getLevel() < level) {
                accounts.addAll(acc.getFollowers());
            }
        }

        List<Character> list = new ArrayList<>(counts.keySet());
        Collections.sort(list);

        double totalCount = counts.values().stream().reduce(Integer::sum).orElse(0);
        for (Character key : list) {
            System.out.printf("%s: %d, %.2f%%%n", key, counts.get(key), (counts.get(key) / totalCount) * 100);
        }
    }


    private static void addToCounts(int followerCount, Map<Character, Integer> counts) {
        Character firstDigit = firstDigit(followerCount);
        if (counts.containsKey(firstDigit)) {
            Integer count = counts.get(firstDigit);
            counts.put(firstDigit, count + 1);
        } else {
            counts.put(firstDigit, 1);
        }
    }

    private static Character firstDigit(Integer count) {
        return count.toString().toCharArray()[0];
    }


}




