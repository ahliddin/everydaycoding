package tj.durud.fakeinstagramdetector;

import org.apache.commons.lang3.RandomUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AccountUtils {
    public static List<Account> generateRandomFollowers(Account parent) {
        int numberOfFollowers = RandomUtils.nextInt(1, 10000);
        System.out.println("random number of followers: " + numberOfFollowers);
        return Stream.generate(() -> createRandomAccount(parent))
                .limit(numberOfFollowers)
                .collect(Collectors.toList());
    }

    public static Account createRandomAccount(Account parent) {
        return new Account(parent, parent.getLevel() + 1);
    }
}